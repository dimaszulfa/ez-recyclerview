package com.ramanaptr.sampleezrecyclerview

import java.io.Serializable

data class SampleData(
    var key: String,
    var value: String,
) : Serializable
